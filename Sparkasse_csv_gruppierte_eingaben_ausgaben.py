import sys, csv, locale, tabulate
from datetime import datetime
from collections import Counter
import numpy as np, matplotlib.pyplot as plt

from config import *

# input data format
value_column = 'Betrag'
locale.setlocale(locale.LC_ALL, 'de_DE')
actor_column= 'Beguenstigter/Zahlungspflichtiger'
date_column= 'Buchungstag'
date_format= '%d.%m.%y'
message_column = "Verwendungszweck"

# display parameters
month_format = '%y %b'
topic_column = "Kategorie"
clean_columns = [date_column, value_column, "Waehrung", topic_column, actor_column, message_column]

important_actor_count = 16 # amount of actors shown in monthly table
table_head_len = 7 # truncate table headers

other_income_label = "+ other"
other_outgoing_label = "- other"
incoming_label = "+ total"
outgoing_label = "- total"
total_label = "Bilanz" 

                      

# debugging
debug_logging = False
def debug(*string):
    if debug_logging:
        print(' Debug:', *string);

def Topic_or_actor(actor):
    if actor in actor_topics:
        return actor_topics[actor]
    else:
        return "*" + actor
def Topic(actor):
    if actor in actor_topics:
        return actor_topics[actor]
    else:
        return ''
def Table_row(transaction):
    return [transaction[column] for column in clean_columns]
def print_transactions(transactions, header=clean_columns):
    rows = [[transaction[key] for key in header] for transaction in transactions]
    print()
    print(tabulate.tabulate(rows, header)) 

print('\nrunning "' + sys.argv[0]+ '" with "' + bank_csv_file + '"\n');

## read file
transactions = []
actors = {}
header = []

with open(bank_csv_file) as input_file:
    csv_reader = csv.reader(input_file, delimiter=';', quotechar='"')
    try:
        header = next(csv_reader)
        debug(header)
        debug_counter = 1
        for row in csv_reader:
            transaction = dict(zip(header, row))
            # type conversions, clean up
            transaction[value_column] = float(locale.atof(transaction[value_column]))
            transaction['datetime'] = datetime.strptime(transaction[date_column], date_format)
            transaction['month'] = transaction["datetime"].strftime(month_format)
            transaction[message_column] = transaction[message_column][:50]
            transaction[topic_column] = Topic(transaction[actor_column])
            
            if debug_logging and (debug_counter > 0):
                print(row)
                print_transactions([transaction])
                debug_counter -= 1
            
            transactions.append(transaction)
            
            actor = transaction[actor_column]
            if actor in actors:
                actors[actor][1].append(transaction)
            else:
                actors[actor] = [0, [transaction]]
    except csv.Error as e:
        sys.exit('Could not read bank csv file - Error at {}, line {}: {}'.format(filename, reader.line_num, e))


## important topics/actors
topic_abs_sum = {}
topic_sum = {}
for actor in actors.keys():
    topic = Topic_or_actor(actor)
    actor_abs_sum = sum([abs(transaction[value_column]) for transaction in actors[actor][1]])
    actor_sum = sum([transaction[value_column] for transaction in actors[actor][1]])
    if not(topic in topic_abs_sum.keys()):
        topic_abs_sum[topic] = actor_abs_sum
        topic_sum[topic] = actor_sum
    else:
        topic_abs_sum[topic] += actor_abs_sum
        topic_sum[topic] += actor_sum
topicCounter = Counter(topic_abs_sum)
important_topics = [actor for actor, value in topicCounter.most_common(important_actor_count)] 
important_incoming_topics = list(filter(lambda actor: topic_sum[actor] > 0, important_topics))
important_outgoing_topics = list(filter(lambda actor: topic_sum[actor] < 0, important_topics))
print()
table_rows = [[i+1, topic_sum[actor], (abs_sum-abs(topic_sum[actor]))/2, actor] for i, (actor, abs_sum) in enumerate(topicCounter.most_common(70))]
print(tabulate.tabulate(table_rows, ["Nr.", "Summe", "Durchlauf", "Kategorie/*(" + actor_column + ")"]))

## monthly report
monthly_header = ['month', *important_incoming_topics, other_income_label, incoming_label, *important_outgoing_topics, other_outgoing_label, outgoing_label, total_label]
#monthly_header = ['month', *important_outgoing_topics, other_outgoing_label, outgoing_label, incoming_label, total_label]
monthly_header_short = [label[:table_head_len] for label in monthly_header]
months = []
monthly_values = {}
for transaction in transactions:
    month = transaction['month']
    topic = Topic_or_actor(transaction[actor_column])
    value = transaction[value_column]
    if not (month in months):
        months.append(month)
        monthly_values[month] = np.zeros(len(monthly_header)-1)
    if topic in monthly_header:
        i = monthly_header.index(topic) -1
        monthly_values[month][i] += value
    else:
        if (value > 0) and (other_income_label in monthly_header):
            monthly_values[month][monthly_header.index(other_income_label) -1] += value
        elif (value < 0) and (other_outgoing_label in monthly_header):
            monthly_values[month][monthly_header.index(other_outgoing_label) -1] += value
    if (value > 0) and (incoming_label in monthly_header):
        monthly_values[month][monthly_header.index(incoming_label) -1] += value
    elif (value < 0) and (outgoing_label in monthly_header):
        monthly_values[month][monthly_header.index(outgoing_label) -1] += value
    if total_label in monthly_header:
        monthly_values[month][monthly_header.index(total_label) -1] += value
#Average
monthly_average = np.zeros(len(monthly_header)-1)
for month in months:
    monthly_average += monthly_values[month]
monthly_average = monthly_average / len(months)
months.append("average")
monthly_values["average"] = monthly_average

#round
for month in months:
    monthly_values[month] = list(np.round_(monthly_values[month]))
    
monthly_rows = [[month, *monthly_values[month]] for month in months]
debug(monthly_rows)
print()
print(tabulate.tabulate(monthly_rows, monthly_header_short))

## example query
kategorie = ""
if 1:
    print()
    print("Custom Query (Kategorie=='{}'):".format(kategorie))
    print()
    table_rows = [Table_row(transaction) for transaction in filter(lambda x: x[topic_column] == kategorie, transactions)]
    print(tabulate.tabulate(table_rows, clean_columns))
actor = ""
if actor != "":
    print()
    print("Custom Query (Beguenstigter/Zahlungspflichtiger=='{}'):".format(actor))
    print()
    table_rows = [Table_row(transaction) for transaction in filter(lambda x: x[actor_column] == actor, transactions)]
    print(tabulate.tabulate(table_rows, clean_columns))
month = ""
if month != "":
    print()
    print("Custom Query (Monat=='{}'):".format(month))
    print()
    table_rows = [Table_row(transaction) for transaction in filter(lambda x: x['month'] == month, transactions)]
    print(tabulate.tabulate(table_rows, clean_columns))
