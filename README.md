# "Buchführung" für Sparkassen-Konten (Command line)

Ein Python-Skript, das es ermöglicht, die Eingaben und Ausgaben von einem Sparkassen-Konto zu überblicken.
Die Daten kommen aus der herunterladbaren CSV-Datei. Die Ausgabe ist ausschließlich in der Kommandozeile.

Das Skript enthält (jeweils als Tabelle):
- eine Übersicht über alle fremden Konten (soweit möglich, nach Kategorie gruppiert), sortiert nach Umsatz 
- eine monatliche Übersicht mit:
	- den finanzstärksten Kategorien / fremden Konten
	- der Summe der Ausgaben / Einnahmen sowie monatlicher Bilanz
	- den monatlichen Durchschnitt im gesamten betrachteten Zeitraum
- einer Auflistung aller Buchungen in einer Query (Im default: keiner Kategorie zugeordnet)


## Benutzung
Die `default-config.py` muss kopiert werden und in `config.py` umbenannt werden.
Für eine gute Übersicht müssen den Konten Kategorien zugewiesen werden. Dies erfolgt über ein dictionary in `config.py`.
